#include "Wrapper.h"
#include "Logger.h"

// Implementation of set log file location
LIB_API void SetLogFileLoc(const char* fileName) {	
	// Try and open the file
	FILE* file = _fsopen(fileName, "w", SH_DENYNO);

	// As long as the file opened
	if (file != nullptr) {
		// If there is a file already bound, close it
		if (Output2FILE::Stream() != nullptr) {
			fclose(Output2FILE::Stream());
			Output2FILE::Stream() = stderr;
		}
		// Bind the file
		Output2FILE::Stream() = file;
	}
}

LIB_API void NativeLog(int level, const char* text) {
	FILE_LOG((TLogLevel)level) << text;
}

LIB_API HistoryContext * CreateHistory() {
	return new HistoryContext();
}

LIB_API void DeleteHistory(HistoryContext * history) {
	delete history;
}

LIB_API TileMap* CreateMap() {
	return new TileMap();
}

LIB_API void DeleteMap(TileMap * map) {
	delete map;
}

LIB_API void SetTile(TileMap * map, HistoryContext * history, uint16_t x, uint16_t y, uint8_t tileID)
{
	PlaceCommand* command = new PlaceCommand();
	command->TileX = x;
	command->TileY = y;
	command->TileID = tileID;
	history->Push(command, map);
}

LIB_API uint8_t GetTile(TileMap * map, HistoryContext * history, uint16_t x, uint16_t y) {
	return map->GetTile(x, y);
}

LIB_API void Undo(TileMap * map, HistoryContext * history) {
	history->Undo(map);
}

LIB_API void Redo(TileMap * map, HistoryContext * history) {
	history->Redo(map);
}

