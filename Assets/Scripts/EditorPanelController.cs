﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class EditorPanelController : MonoBehaviour {

    public RectTransform EditorPanel;
    public RectTransform EditorPanelToggle;
    public bool EditorVisible
    {
        get { return __EditorVisible; }
        set { SetEditorVisible(value); }
    }
    private bool __EditorVisible = true;
    
    public void SetEditorVisible(bool visible)
    {
        if (EditorVisible != visible)
        {
            if (!visible)
            {
                EditorPanel.position = new Vector3(Screen.width + EditorPanel.rect.width / 2, EditorPanel.position.y);
                EditorPanelToggle.position = new Vector3(Screen.width - EditorPanelToggle.rect.width, Screen.height);
                EditorPanelToggle.rotation = Quaternion.Euler(0, 0, -90);
            }
            else
            {
                EditorPanel.position = new Vector3(Screen.width - EditorPanel.rect.width, EditorPanel.position.y);
                EditorPanelToggle.position = new Vector3(Screen.width - EditorPanel.rect.width, Screen.height - EditorPanelToggle.rect.height);
                EditorPanelToggle.rotation = Quaternion.Euler(0, 0, 90);
            }
            __EditorVisible = visible;
        }
    }

    public void ToggleEditPanel()
    {
        SetEditorVisible(!EditorVisible);
    }
}
