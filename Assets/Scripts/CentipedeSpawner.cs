﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CentipedeSpawner : MonoBehaviour {

    public GameObject centipedeSegment;
    public int spawnAmount = 10;
    public float spawnDelay = 0.5f;
    private bool spawned = false;

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {

        if (Static.startGame && !spawned)
        {
            spawned = true;
            StartCoroutine("SpawnSegments");
        }
        // kill
		if (spawnAmount <= 0)
        {
            GameObject.Destroy(gameObject);
            // dia
        }
	}

    //
    IEnumerator SpawnSegments()
    {
        do
        {
            GameObject.Instantiate(centipedeSegment, transform.position, Quaternion.identity);
            spawnAmount--;
            yield return new WaitForSeconds(spawnDelay);
        } while (spawnAmount > 0);
    }
}
