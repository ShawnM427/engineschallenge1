using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.InteropServices;
using System;

namespace CentipedeCore
{
    public enum TileID : byte {
        None      = 0,
        Player    = 1,
        Tree      = 2,
        Spider    = 3,
        Centipede = 4
    };

    static class Config
    {
        public const string DLL_NAME = "CentipedeCore";
    }

    public static class Logger
    {
        [DllImport(Config.DLL_NAME)]
        public static extern void SetLogFileLoc(string fileName);
        [DllImport(Config.DLL_NAME)]
        public static extern void NativeLog(int level, string text);
        
        static Logger() {
            SetLogFileLoc("logs.txt");
        }

        public static void Touch() { NativeLog(2, "Ensuring logger exists..."); }
    }

    public class TileMap
    {
        #region Interop
        [DllImport(Config.DLL_NAME)]
        private static extern System.IntPtr CreateHistory();

        [DllImport(Config.DLL_NAME)]
        private static extern void DeleteHistory(System.IntPtr history);

        [DllImport(Config.DLL_NAME)]
        private static extern System.IntPtr CreateMap();

        [DllImport(Config.DLL_NAME)]
        private static extern void DeleteMap(System.IntPtr history);

        [DllImport(Config.DLL_NAME)]
        private static extern void SetTile(System.IntPtr map, System.IntPtr history, ushort x, ushort y, byte tileID);

        [DllImport(Config.DLL_NAME)]
        private static extern byte GetTile(System.IntPtr map, System.IntPtr history, ushort x, ushort y);

        [DllImport(Config.DLL_NAME)]
        private static extern void Undo(System.IntPtr map, System.IntPtr history);

        [DllImport(Config.DLL_NAME)]
        private static extern void Redo(System.IntPtr map, System.IntPtr history);
        #endregion

        private System.IntPtr myMapHandle;
        private System.IntPtr myHistoryHandle;

        public TileMap() {
            Logger.Touch();
            myMapHandle = CreateMap();
            myHistoryHandle = CreateHistory();
        }

        ~TileMap() {
            try
            {
                Logger.NativeLog(2, "Cleaning up map");
                DeleteMap(myMapHandle);
                myMapHandle = System.IntPtr.Zero;
                Logger.NativeLog(2, "Cleaning up history context");
                DeleteHistory(myHistoryHandle);
                myHistoryHandle = System.IntPtr.Zero;
            }
            catch (Exception e) { }
        }

        public void Set(ushort x, ushort y, TileID tileId) {
            if (x < 25 && y < 20) {
                SetTile(myMapHandle, myHistoryHandle, x, y, (byte)tileId);
            }
        }

        public TileID Get(ushort x, ushort y)
        {
            if (x < 25 && y < 20) {
                return (TileID)GetTile(myMapHandle, myHistoryHandle, x, y);
            }  else {
                return TileID.None;
            }
        }

        public void Undo() {
            Undo(myMapHandle, myHistoryHandle);
        }

        public void Redo() {
            Redo(myMapHandle, myHistoryHandle);
        }
    }

}